#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;

#pragma region Classes
class Computing
{
    public:
    string Results;int *DNsolutions;string *Combinations;bool *Symbol;
    bool Show_Combinations=true;//Show/Hide The Combinations on output
    void Compute(int M,string Dataset[])
	{
		for (int i = 0; i < M; i++)
		{
		    int K=stoi(Dataset[i].substr(0, Dataset[i].find(" ")));
            string *data;data=new string[K+2];
            data = split(Dataset[i],K," ");
            int *Digits;Digits=new int[K];
            int Total=stoi(data[K+1]);
            for (int i1=0;i1<K;i1++)
            {
                Digits[i1] = stoi(data[i1+1]);
            }
            //------------------------------
            DNsolutions=new int[Total+1];
            Combinations=new string[Total+1];
            Symbol=new bool[Total+1];
            DNconcatenating(Digits,Total,K);
            for(int i2=1;i2<=Total;i2++)
            {
                string comb1=Multiply(Digits,i2,K);
                string comb2=Sum(Digits,i2,K);
                string comb3=SumPlus(i2);
                string comb4=MultiplyPlus(i2);
                int ccomb1=comb1.length();
                int ccomb2=comb2.length();
                int ccomb3=comb3.length();
                int ccomb4=comb4.length();
                if (ccomb1==0)ccomb1=1000000;if (ccomb2==0)ccomb2=1000000;if (ccomb3==0)ccomb3=1000000;if (ccomb4==0)ccomb4=1000000;
                if (ccomb1!=1000000 && ccomb1<=ccomb2 && ccomb1<=ccomb3 && ccomb1<=ccomb4)
                {
                    DNsolutions[i2]=ccomb1;
                    Combinations[i2]=comb1;
                }else
                if (ccomb2!=1000000 && ccomb2<=ccomb1 && ccomb2<=ccomb3 && ccomb2<=ccomb4)
                {
                    DNsolutions[i2]=ccomb2;
                    Combinations[i2]=comb2;
                    Symbol[i2]=true;
                }else
                if (ccomb3!=1000000 && ccomb3<=ccomb1 && ccomb3<=ccomb2 && ccomb3<=ccomb4)
                {
                    DNsolutions[i2]=ccomb3;
                    Combinations[i2]=comb3;
                    Symbol[i2]=true;
                }else
                if (ccomb4!=1000000 && ccomb4<=ccomb1 && ccomb4<=ccomb2 && ccomb4<=ccomb3)
                {
                    DNsolutions[i2]=ccomb4;
                    Combinations[i2]=comb4;
                }
            }
            if (Show_Combinations)
            {
                if (DNsolutions[Total]>0) Results=Results+to_string(DNsolutions[Total])+" | "+Combinations[Total]+"\n";else Results=Results+"N\n";
            }
            else
            {
                if (DNsolutions[Total]>0) Results=Results+to_string(DNsolutions[Total])+"\n";else Results=Results+"N\n";
            }
		}
		//cout<<Results;
	}
	string *split(string str,int k,const string splitter)
	{
        string *result;result=new string[k+2];int c=0;int pos1;
        while(str.length()>0)
        {
            pos1=str.find(splitter);
            if (pos1>=0)
            {
                result[c]=str.substr(0, pos1);
                str.erase(0,pos1+1);
            }
            else
            {
                result[c]=str;
                str="";
            }
            c++;
        }
        return result;
    }
    private:
	void DNconcatenating(int digits[],int total,int k)
	{
        for(int i=1;i<=total;i++)
        {
           DNsolutions[i]=Compare(i,digits,k);
        }
	}
	int Compare(int x,int d[],int k)
	{
	    string s=to_string(x);string result;
	    for(int i=0;i<s.length();i++)
        {
            bool C=false;
            for(int i1=0;i1<k;i1++)
            {
                if (d[i1]==s[i]- '0')
                {
                    C=true;
                    break;
                }
            }
            if (C==false)
                return 0;
        }
        return s.length();
	}
	string Multiply(int digits[],int total,int k)
	{
        int result=0;string comb;
	    for(int i=0;i<k;i++)
        {
            if (digits[i]>0)
            {
                int r=total/digits[i];
                int x=total%digits[i];
                if (x==0 && DNsolutions[r]>0 && Symbol[r]==false)
                {
                    int temp=DNsolutions[r]+1+to_string(digits[i]).length();
                    if (result==0 || temp<result)
                    {
                        result = temp;
                        if (DNsolutions[total]<=0 || result<DNsolutions[total])
                        {
                            if (Combinations[r]=="")
                            comb=to_string(digits[i])+"*"+to_string(r);
                            else comb=to_string(digits[i])+"*"+Combinations[r];
                        }
                    }
                }
            }
        }
        return comb;
	}
	string Sum(int digits[],int total,int k)
	{
        int result=0;string comb;
	    for(int i=0;i<k;i++)
        {
            if (digits[i]<total)
            {
                if (DNsolutions[total-digits[i]]>0)
                {
                    int temp=DNsolutions[total-digits[i]]+1+to_string(digits[i]).length();
                    if (result==0 || temp<result)
                    {
                        result = temp;
                        if (DNsolutions[total]<=0 || result<DNsolutions[total])
                        {
                            if (Combinations[total-digits[i]]=="")
                            comb=to_string(digits[i])+"+"+to_string(total-digits[i]);
                            else comb=to_string(digits[i])+"+"+Combinations[total-digits[i]];
                        }
                    }
                }
            }
        }
        return comb;
	}
	string SumPlus(int total)
	{
	    int result=0;string comb;string x1;string x2;
        for (int i1 = 1; i1 <= total; i1++)
        {
            if (DNsolutions[total - i1] > 0 && DNsolutions[i1] > 0)
            {
                int temp = DNsolutions[i1] + 1 + DNsolutions[total - i1];
                if (result==0 || temp<result)
                {
                    result = temp;
                    if (DNsolutions[total]<=0 || result<DNsolutions[total])
                    {
                        if (Combinations[total-i1]=="") x2=to_string(total-i1);
                        else x2=Combinations[total-i1];
                        if (Combinations[i1]=="") x1=to_string(i1);
                        else x1=Combinations[i1];
                        comb=x1+"+"+x2;
                    }
                }
            }
        }
        return comb;
	}
    string MultiplyPlus(int total)
	{
	    int result=0;string comb;string x1;string x2;
        for (int i1 = 1; i1 <= total; i1++)
        {
            int r=total/i1;
            int x=total%i1;
            if (x==0 && DNsolutions[i1] > 0 && DNsolutions[r] > 0 && Symbol[i1]==false && Symbol[r]==false)
            {
                int temp = DNsolutions[i1] + 1 + DNsolutions[r];
                if (result==0 || temp<result)
                {
                    result = temp;
                    if (DNsolutions[total]<=0 || result<DNsolutions[total])
                    {
                        if (Combinations[r]=="") x2=to_string(r);
                        else x2=Combinations[r];
                        if (Combinations[i1]=="") x1=to_string(i1);
                        else x1=Combinations[i1];
                        comb=x1+"*"+x2;
                    }
                }
            }
        }
        return comb;
	}
};
class IO
{
    public:
	void Read(const char * InputFile,const char * OutputFile)
	{
		Computing computing;
		string input;
		fstream fp(InputFile);
		if(fp)
		{
			stringstream buffer;
			buffer << fp.rdbuf();
			input = buffer.str();
			int pos=input.find("\n");
			int m = stoi(input.substr(0, pos));
			input.erase(0,pos+1);
			string *dataset;dataset=new string[m];
			for (int i=0;i<m;i++)
			{
				pos=input.find("\n");
				dataset[i]=input.substr(0, pos);
				input.erase(0,pos+1);
			}
			computing.Compute(m,dataset);
			Write(OutputFile,computing.Results,m);
		}
		else
		{
			cerr<<"Input File can not open! Please add your file name >> e.g. in_toy.txt"<<endl;
			exit(1);
		}
	}
	void Write(const char * OutputFile,string result,int m)
	{
        ofstream op;
        op.open (OutputFile);
        if (op)
        {
            op << result;
            op.close();
        }
        else
		{
			cerr<<"Output File can not open! Please add your file name >> e.g. out_toy.txt"<<endl;
			exit(1);
		}
	}
};
#pragma endregion Classes

int main(int argc,const char * argv[])
{
  IO io;
  io.Read(argv[1],argv[2]);
  return 0;
}


